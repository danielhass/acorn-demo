# acorn.io demo app

Deploy app:
```
acorn run -n awesome-acorn .
```

Run app in developer mode (auto-refresh on code changes):
```
acorn run -n awesome-acorn -i .
```
