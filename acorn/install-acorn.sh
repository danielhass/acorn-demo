#!/bin/bash

if ! command -v acorn &> /dev/null
then
    echo "acorn could not be found"
    exit 1
fi

acorn install
